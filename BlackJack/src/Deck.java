import java.util.Arrays;

public class Deck {

	public static final int NUM_CARDS_IN_DECK = 52;
	private Card[] cards;
	private int numCards;

	public Deck(int numDecks) {
		numCards=NUM_CARDS_IN_DECK * numDecks;
		cards = new Card[NUM_CARDS_IN_DECK * numDecks];
		int counter =0;
		Card c;
		for (int j = 0; j < numDecks; j++) {
			for (int i = 1; i <= 13; i++) {
				c= new Card(i,Suit.CLOVERS);
				cards[counter]=c;
				counter++;
				c= new Card(i,Suit.DIAMONDS);
				cards[counter]=c;
				counter++;
				c= new Card(i,Suit.HEARTS);
				cards[counter]=c;
				counter++;
				c= new Card(i,Suit.SPADES);
				cards[counter]=c;
				counter++;
			}
		}
		shuffle();
	}
	
	public boolean isEmpty() {
		if(numCards==0) {
			return true;
		}else {
			return false;
		}
	}
	
	public Card extractCard() {
		numCards--;
		return cards[numCards];
	}
	
	public void shuffle() {
		for(int i =0;i<cards.length;i++ ) {
			int randPos=(int)(Math.random()*cards.length);
			Card temp;
			temp = cards[i];
			cards[i]=cards[randPos];
			cards[randPos]=temp;
		}
	}
	
	@Override
	public String toString() {
		String s = cards[0].toString();
		for(int i = 1;i<cards.length;i++) {
			s+=", " + cards[i];
		}
		return s;
	}
	
}
