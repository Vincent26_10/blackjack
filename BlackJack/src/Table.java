import java.util.*;

public class Table {

	public static final int NUM_DECKS = 6;
	public static final int NIM_BET = 5;

	private Deck deck;
	private Player[] players;
	private Dealer dealer;
	private Scanner input;

	public Table(int numPlayers, Scanner input) {
		deck = new Deck(NUM_DECKS);
		players = new Player[numPlayers];
		dealer = new Dealer();
		this.input = input;
		for (int i = 0; i < players.length; i++) {
			System.out.println("Player number " + (i + 1));
			players[i] = Player.readFromKeyboard(input);
		}
	}

	@Override
	public String toString() {
		String s = dealer.toString() + "\n";
		for (Player p : players) {
			s += p.toString() + "\n";
		}

		return s;

	}

	public boolean play() {
		boolean gameover = false;
		while (!gameover) {
			firstDeal();
			boolean anyoneAlive = true;
			while (anyoneAlive) {
				restOfDeals();
				anyoneAlive = isAnyoneAlive();
			}
			playDealer();
			printWinners();
			resetPlayers();
			dealer.resetPlayer();
			gameover=askExit();
		}
		return true;
	}
	
	private boolean askExit() {
		System.out.println("END GAME? Y/N");
		String answer = input.next();
		if(answer.equalsIgnoreCase("Y")) {
			return true;
		}			
		return false;
	}

	public void resetPlayers() {
		for(Player p: players) {
			p.resetPlayer();
		}
	}

	private void printWinners() {
		System.out.println("RESULTS");
		int valueCardsDealer = dealer.getCardsValue();
		if (dealer.isBusted()) {
			valueCardsDealer = -1;
		}
		System.out.println(dealer);
		if(dealer.isBusted()) {
			System.out.println("BUSTED");
		}
		for (Player player : players) {
			System.err.println(player);
			if (!player.isBroke() && !player.isBusted()) {
				if (player.getCardsValue() > valueCardsDealer) {
					// wins
					System.out.println("You win " + player.getWinnings());
					player.payWinner();
				} else {
					if (player.getCardsValue() == valueCardsDealer) {
						// Even
						System.out.println("You are even");
						player.payEven();
					}else {
						System.out.println("-You lose-");
					}
				}
			}
		}
	}

	private void playDealer() {
		while (!dealer.isBusted() && dealer.getCardsValue() <= 16) {
			deal(dealer);
		}
	}

	private boolean isAnyoneAlive() {
		for (Player p : players) {
			if (p.isAlive()) {
				return  true;
			}
		}
		return false;
	}

	public void restOfDeals() {
		for (Player p : players) {
			if (p.isAlive()) {
				System.out.println(p + " ");
				p.askCheck(input);
				if (!p.isCheck()) {
					deal(p);
				}
				System.out.println(p);
				if (p.isBusted()) {
					System.out.println("---BUSTED--");
				}
			}
		}
	}

	public void firstDeal() {
		for (Player p : players) {
			if (!p.isBroke()) {
				deal(p);
			}
		}
		deal(dealer);
		System.out.println(this);
		for (Player p : players) {
			if (!p.isBroke()) {
				askForBet(p);
				deal(p);
				System.out.println(p);
			}
		}
	}

	private void askForBet(Player p) {
		System.out.println(p.getName() + "enter your bet( " + p.getMoney() + " left)");

		int bet = input.nextInt();
		p.makeBet(bet);

	}

	private void deal(Player p) {
		p.giveCard(deck.extractCard());

	}

}
